# Docker's gitlab

Вообще это все можно уместить в 1 скрипт или автоматизировать при помощи gitlab ci

1. mv ./gitlab/docker-compose.yml /root/ 
2. изменить переменную docker-compose <SERVER_DNS_NAME> 
3.  mkdir /opt/gitlab
    mkdir /opt/gitlab/config
    mkdir /opt/gitlab/logs
    mkdir /opt/gitlab/data
    mkdir /opt/gitlab-runner
    mkdir /opt/gitlab-runner/config
    mkdir /opt/gitlab-runner/data
4.  проверить 22 порт /etc/ssh/sshd_config назначить для ssh другой порт и отдать 22 гиту
    ретарт ssh - service sshd restart
5. docker-compose up -vvvv

Готово!